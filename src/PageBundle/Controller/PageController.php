<?php

namespace PageBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Templating\EngineInterface;

/**
 * @Route("/page")
 */
final class PageController
{
    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * PageController constructor.
     *
     * @param EngineInterface $templating
     */
    public function __construct(EngineInterface $templating)
    {
        $this->templating = $templating;
    }

    /**
     * @Route("/{slug}.html", requirements={"slug" = "[a-zA-Z0-9-_]+"}, name="page")
     * @Method("GET")
     *
     * @param string $slug
     *
     * @return Response
     */
    public function showAction(string $slug): Response
    {
        $templateName = basename(sprintf(':page:%s.html.twig', $slug));

        if (!$this->templating->exists($templateName)) {
            throw new NotFoundHttpException(
                sprintf('Page "%s" not found', $slug));
        }

        $page = $this->templating->render($templateName);

        return new Response($page);
    }
}
