<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TaxRefBundle\Entity\Specie;
use AppBundle\Annotation\Uploadable;
use AppBundle\Annotation\UploadableField;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ObservationRepository")
 * @ORM\Table(name="observations")
 * @Uploadable()
 */
class Observation implements \JsonSerializable
{
    public const STATUS_NEW = 0;
    public const STATUS_APPROVED = 1;
    public const STATUS_DISAPPROVED = 2;
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    protected $latitude;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    protected $longitude;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="observations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="approved_observations")
     * @ORM\JoinColumn(name="approver_id", referencedColumnName="id", nullable=true)
     */
    protected $approver;

    /**
     * @var Specie
     *
     * @ORM\ManyToOne(targetEntity="TaxRefBundle\Entity\Specie")
     * @ORM\JoinColumn(name="specie_id", referencedColumnName="cd_nom")
     */
    protected $specie;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $number;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comment;

    /**
     * @var \Datetime|null
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var \Datetime|null
     *
     * @ORM\Column(type="datetime")
     */
    protected $viewed_at;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private $filename;

    /**
     * @UploadableField(filename="filename", path="uploads")
     * @Assert\Image()
     */
    private $file;

    /**
     * Observation constructor.
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->viewed_at = new \DateTime();
        $this->status = self::STATUS_NEW;
    }

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude(float $latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude(float $longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return Specie
     */
    public function getSpecie(): ?Specie
    {
        return $this->specie;
    }

    /**
     * @param Specie $specie
     */
    public function setSpecie(Specie $specie)
    {
        $this->specie = $specie;
    }

    /**
     * @return int
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber(int $number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param null|string $comment
     */
    public function setComment(?string $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return \Datetime|null
     */
    public function getCreatedAt(): ?\Datetime
    {
        return $this->created_at;
    }

    /**
     * @param \Datetime|null $created_at
     */
    public function setCreatedAt(?\Datetime $created_at)
    {
        $this->created_at = $created_at;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isApproved(): bool
    {
        return self::STATUS_APPROVED === $this->status;
    }

    /**
     * @return User
     */
    public function getApprover(): ?User
    {
        return $this->approver;
    }

    /**
     * @param User $approver
     */
    public function setApprover(?User $approver)
    {
        $this->approver = $approver;
    }

    /**
     * @return \Datetime|null
     */
    public function getViewedAt(): ?\Datetime
    {
        return $this->viewed_at;
    }

    /**
     * @param \Datetime|null $viewed_at
     */
    public function setViewedAt(?\Datetime $viewed_at)
    {
        $this->viewed_at = $viewed_at;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'coordinates' => [
                'lat' => $this->getLatitude(),
                'lng' => $this->getLongitude(),
            ],
            'specie' => $this->getSpecie(),
            'number' => $this->getNumber(),
            'created_at' => $this->getCreatedAt(),
            'viewed_at' => $this->getViewedAt(),
        ];
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }
}
