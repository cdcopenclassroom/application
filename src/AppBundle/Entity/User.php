<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Veuillez indiquer votre prénom.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=2,
     *     max=255,
     *     minMessage="Votre prénom est trop court.",
     *     maxMessage="Votre prénom est trop  long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $firstname;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Veuillez indiquer votre nom.", groups={"Registration", "Profile"})
     * @Assert\Length(
     *     min=2,
     *     max=255,
     *     minMessage="Votre nom est trop court.",
     *     maxMessage="Votre nom est trop  long.",
     *     groups={"Registration", "Profile"}
     * )
     */
    protected $lastname;

    /**
     * @param Collection
     *
     * @ORM\OneToMany(targetEntity="Observation", mappedBy="user")
     */
    protected $observations;

    /**
     * @param Collection
     *
     * @ORM\OneToMany(targetEntity="Observation", mappedBy="approver")
     */
    protected $approved_observations;

    public function __construct()
    {
        parent::__construct();

        $this->observations = new ArrayCollection();
        $this->approved_observations = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->getFullname() ?? $this->getUsername();
    }

    /**
     * @return null|string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param null|string $firstname
     */
    public function setFirstname(?string $firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return null|string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param null|string $lastname
     */
    public function setLastname(?string $lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return null|string
     */
    public function getFullname(): ?string
    {
        if (!empty($this->getFirstname()) &&!empty($this->getLastname())) {
            return sprintf('%s %s', $this->getFirstname(), $this->getLastname());
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isNaturalist(): bool
    {
        return in_array('ROLE_NATURALIST', $this->getRoles());
    }

    /**
     * @return Collection
     */
    public function getObservations(): Collection
    {
        return $this->observations;
    }

    /**
     * @param Observation $observation
     */
    public function addObservation(Observation $observation)
    {
        $this->observations->add($observation);
    }

    /**
     * @param Observation $observation
     */
    public function removeObservation(Observation $observation)
    {
        $this->observations->removeElement($observation);
    }

    /**
     * @return Collection
     */
    public function getApprovedObservations(): Collection
    {
        return $this->approved_observations;
    }

    /**
     * @param Observation $observation
     */
    public function addApprovedObservation(Observation $observation)
    {
        $this->approved_observations->add($observation);
    }

    /**
     * @param Observation $observation
     */
    public function removeApprovedObservation(Observation $observation)
    {
        $this->approved_observations->removeElement($observation);
    }
}
