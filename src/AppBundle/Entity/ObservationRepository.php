<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

final class ObservationRepository extends EntityRepository
{
    /**
     * @param array $query
     *
     * @return Pagerfanta
     */
    public function listAllObservationsWithPagination(array $query): Pagerfanta
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Observation::class, 'o')
            ->orderBy('o.viewed_at', 'DESC');
        $this->filterQuery($queryBuilder, $query);
        $adapter = new DoctrineORMAdapter($queryBuilder);

        return new Pagerfanta($adapter);
    }

    /**
     * @param array $query
     *
     * @return Observation[]
     */
    public function listAllApprovedObservations(array $query): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Observation::class, 'o')
            ->orderBy('o.viewed_at', 'DESC');
        $this->filterQuery($queryBuilder, $query);

        return $queryBuilder
            ->getQuery()
            ->execute();
    }

    /**
     * @param array $query
     *
     * @return Pagerfanta
     */
    public function listAllObservationsPendingApprovalWithPagination(array $query): Pagerfanta
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Observation::class, 'o')
            ->andWhere('o.status = :status')
            ->setParameter('status', Observation::STATUS_NEW)
            ->orderBy('o.created_at', 'DESC');
        $this->filterQuery($queryBuilder, $query);
        $adapter = new DoctrineORMAdapter($queryBuilder);

        return new Pagerfanta($adapter);
    }

    /**
     * @param array $query
     *
     * @return Pagerfanta
     */
    public function listAllRefusedObservationsWithPagination(array $query): Pagerfanta
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Observation::class, 'o')
            ->andWhere('o.status = :status')
            ->setParameter('status', Observation::STATUS_DISAPPROVED)
            ->orderBy('o.created_at', 'DESC');
        $this->filterQuery($queryBuilder, $query);
        $adapter = new DoctrineORMAdapter($queryBuilder);

        return new Pagerfanta($adapter);
    }

    /**
     * @param array $query
     *
     * @return Pagerfanta
     */
    public function listAllApprovedObservationsWithPagination(array $query): Pagerfanta
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Observation::class, 'o')
            ->andWhere('o.status = :status')
            ->setParameter('status', Observation::STATUS_APPROVED)
            ->orderBy('o.viewed_at', 'DESC');
        $this->filterQuery($queryBuilder, $query);

        $adapter = new DoctrineORMAdapter($queryBuilder);

        return new Pagerfanta($adapter);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array $query
     */
    private function filterQuery(QueryBuilder $queryBuilder, array $query)
    {
        $coordinatesFilters = $this->filterCoordinates($query);
        if (!empty($coordinatesFilters)) {
            foreach ($coordinatesFilters as $key => $value) {
                switch ($key) {
                    case 'latSup':
                        $queryBuilder->andWhere("o.latitude <= :latSup")->setParameter('latSup', $value);
                        break;
                    case 'latSub':
                        $queryBuilder->andWhere("o.latitude >= :latSub")->setParameter('latSub', $value);
                        break;
                    case 'lonSup':
                        $queryBuilder->andWhere("o.longitude <= :lonSup")->setParameter('lonSup', $value);
                        break;
                    case 'lonSub':
                        $queryBuilder->andWhere("o.longitude >= :lonSub")->setParameter('lonSub', $value);
                        break;
                }
            }
        }
        $specieFilters = $this->filterSpecie($query);
        if (!empty($specieFilters)) {
            $queryBuilder->leftJoin('o.specie', 's');
            foreach ($specieFilters as $key => $value) {
                $queryBuilder
                    ->andWhere("s.$key = :$key")
                    ->setParameter($key, $value);
            }
        }
        $specieVernacularName = $this->filterVernacularName($query);
        if (!empty($specieVernacularName)) {
            $queryBuilder->leftJoin('o.specie', 's');
            foreach ($specieVernacularName as $key => $value) {
                $queryBuilder
                    ->andWhere("s.$key LIKE :$key")
                    ->setParameter($key, '%'.$value.'%');
            }
        }
    }

    /**
     * @param array $query
     * @return array
     */
    private function filterCoordinates(array $query): array
    {
        return array_filter(
            $query,
            function ($value, $key) {
                return in_array($key, ['latSup', 'latSub', 'lonSup', 'lonSub']) && !empty($value);
            },
            ARRAY_FILTER_USE_BOTH
        );
    }

    /**
     * @param array $query
     * @return array
     */
    private function filterVernacularName(array $query): array
    {
        return array_filter(
            $query,
            function ($value, $key) {
                return in_array($key, ['vernacular_name']) && !empty($value);
            },
            ARRAY_FILTER_USE_BOTH
        );
    }

    /**
     * @param array $query
     * @return array
     */
    private function filterSpecie(array $query): array
    {
        return array_filter(
            $query,
            function ($value, $key) {
                return in_array($key, ['cd_nom', 'ordre', 'family', 'habitat']) && !empty($value);
            },
            ARRAY_FILTER_USE_BOTH
        );
    }

    /**
     * @param string $order
     *
     * @return Observation[]
     */
    public function listAllApprovedWithOrder(string $order): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Observation::class, 'o')
            ->leftJoin('o.specie', 's')
            ->where('s.ordre = :order')
            ->andWhere('o.status = :status')
            ->setParameter('status', Observation::STATUS_APPROVED)
            ->setParameter('order', $order);

        return $queryBuilder
            ->getQuery()
            ->execute();
    }

    /**
     * @param string $family
     *
     * @return Observation[]
     */
    public function listAllApprovedWithFamily(string $family): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Observation::class, 'o')
            ->leftJoin('o.specie', 's')
            ->where('s.family = :family')
            ->andWhere('o.status = :status')
            ->setParameter('status', Observation::STATUS_APPROVED)
            ->setParameter('family', $family);

        return $queryBuilder
            ->getQuery()
            ->execute();
    }

    /**
     * @param string $cd_nom
     *
     * @return Observation[]
     */
    public function listAllApprovedWithCdNom(string $cd_nom): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('o')
            ->from(Observation::class, 'o')
            ->leftJoin('o.specie', 's')
            ->where('s.cd_nom = :cd_nom')
            ->andWhere('o.status = :status')
            ->setParameter('status', Observation::STATUS_APPROVED)
            ->setParameter('cd_nom', $cd_nom);

        return $queryBuilder
            ->getQuery()
            ->execute();
    }
}
