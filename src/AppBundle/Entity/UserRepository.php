<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

final class UserRepository extends EntityRepository
{
    /**
     * @return Pagerfanta
     */
    public function listAllUsersWithPagination(): Pagerfanta
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u');
        $adapter = new DoctrineORMAdapter($queryBuilder);

        return new Pagerfanta($adapter);
    }

    /**
     * @param string $role
     *
     * @return Pagerfanta
     */
    public function listUsersWithRoleAndPagination(string $role): Pagerfanta
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.roles LIKE :role')
            ->setParameter('role', '%"'.$role.'"%');
        $adapter = new DoctrineORMAdapter($queryBuilder);

        return new Pagerfanta($adapter);
    }
}
