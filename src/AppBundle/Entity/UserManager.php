<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Doctrine\UserManager as BaseUserManager;
use Pagerfanta\Pagerfanta;

final class UserManager extends BaseUserManager
{
    /**
     * @return Pagerfanta
     */
    public function listAllUsersWithPagination(): Pagerfanta
    {
        return $this->getRepository()->listAllUsersWithPagination();
    }

    /**
     * @param string $role
     *
     * @return Pagerfanta
     */
    public function listUsersWithRoleAndPagination(string $role): Pagerfanta
    {
        return $this->getRepository()->listUsersWithRoleAndPagination($role);
    }
}
