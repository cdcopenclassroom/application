<?php

namespace AppBundle\Entity;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Pagerfanta;

final class ObservationManager
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var ManagerRegistry
     */
    private $registry;

    /**
     * ObservationManager constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = Observation::class;
        $this->registry = $registry;
    }

    /**
     * @return Observation[]
     */
    public function findAll(): array
    {
        return $this->getEntityRepository()->findAll();
    }

    /**
     * @param string $id
     * @return Observation|null
     */
    public function findOneById(string $id): ?Observation
    {
        return $this->getEntityRepository()->findOneBy(['id' => $id]);
    }

    /**
     * @param array $query
     *
     * @return Pagerfanta
     */
    public function listAllObservationsWithPagination(array $query): Pagerfanta
    {
        return $this->getEntityRepository()->listAllObservationsWithPagination($query);
    }

    /**
     * @param array $query
     *
     * @return Observation[]
     */
    public function listAllApprovedObservations(array $query): array
    {
        return $this->getEntityRepository()->listAllApprovedObservations($query);
    }

    /**
     * @param array $query
     *
     * @return Pagerfanta
     */
    public function listAllApprovedObservationsWithPagination(array $query): Pagerfanta
    {
        return $this->getEntityRepository()->listAllApprovedObservationsWithPagination($query);
    }

    /**
     * @param array $query
     * @return Pagerfanta
     */
    public function listAllObservationsPendingApprovalWithPagination(array $query): Pagerfanta
    {
        return $this->getEntityRepository()->listAllObservationsPendingApprovalWithPagination($query);
    }

    /**
     * @param array $query
     *
     * @return Pagerfanta
     */
    public function listAllRefusedObservationsWithPagination(array $query): Pagerfanta
    {
        return $this->getEntityRepository()->listAllRefusedObservationsWithPagination($query);
    }

    /**
     * @param string $order
     *
     * @return Observation[]
     */
    public function listAllApprovedWithOrder(string $order): array
    {
        return $this->getEntityRepository()->listAllApprovedWithOrder($order);
    }

    /**
     * @param string $family
     *
     * @return Observation[]
     */
    public function listAllApprovedWithFamily(string $family): array
    {
        return $this->getEntityRepository()->listAllApprovedWithFamily($family);
    }

    /**
     * @param string $cd_nom
     *
     * @return Observation[]
     */
    public function listAllApprovedWithCdNom(string $cd_nom): array
    {
        return $this->getEntityRepository()->listAllApprovedWithCdNom($cd_nom);
    }

    /**
     * @param Observation $observation
     * @param bool $flush
     *
     * @throws \Exception
     */
    public function save(Observation $observation, bool $flush = true)
    {
        try {
            $this->getEntityManager()->getConnection()->beginTransaction();
            $this->getEntityManager()->persist($observation);
            if ($flush) {
                $this->flush();
            }
            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            throw $e;
        }
    }

    /**
     * @param Observation $observation
     * @param bool $flush
     *
     * @throws \Exception
     */
    public function delete(Observation $observation, bool $flush = true)
    {
        try {
            $this->getEntityManager()->getConnection()->beginTransaction();
            $this->getEntityManager()->remove($observation);
            if ($flush) {
                $this->flush();
            }
            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            throw $e;
        }
    }

    /**
     *
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    /**
     * @return EntityManagerInterface
     */
    private function getEntityManager(): EntityManagerInterface
    {
        return $this->registry->getManagerForClass($this->class);
    }

    /**
     * @return ObservationRepository
     */
    private function getEntityRepository(): ObservationRepository
    {
        return $this->getEntityManager()->getRepository($this->class);
    }
}
