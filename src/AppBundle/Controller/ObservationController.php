<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Observation;
use AppBundle\Entity\ObservationManager;
use AppBundle\Form\Handler\ObservationFormHandler;
use TaxRefBundle\Entity\Specie;
use TaxRefBundle\Entity\SpecieManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Templating\EngineInterface;

/**
 * @Route("/observation")
 */
final class ObservationController
{
    /**
     * @var ObservationManager
     */
    private $observationManager;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var ObservationFormHandler
     */
    private $observationFormHandler;

    /**
     * ObservationController constructor.
     *
     * @param Session $session
     * @param ObservationFormHandler $observationFormHandler
     * @param TokenStorageInterface $tokenStorage
     * @param RouterInterface $router
     * @param ObservationManager $observationManager
     * @param EngineInterface $templating
     */
    public function __construct(Session $session, ObservationFormHandler $observationFormHandler, TokenStorageInterface $tokenStorage, RouterInterface $router, ObservationManager $observationManager, SpecieManager $specieManager, EngineInterface $templating)
    {
        $this->session = $session;
        $this->observationFormHandler = $observationFormHandler;
        $this->tokenStorage = $tokenStorage;
        $this->observationManager = $observationManager;
        $this->specieManager = $specieManager;
        $this->templating = $templating;
        $this->router = $router;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/list", name="observation_list")
     * @Method("GET")
     */
    
    public function listAllApprovedObservationsAction(Request $request): Response
    {
        $currentPage = (int) $request->query->get('page', 1);
        $pager = $this->observationManager->listAllApprovedObservationsWithPagination($request->query->all());
        $pager->setMaxPerPage(25);
        $pager->setCurrentPage($currentPage);

        $orders = $this->specieManager->findOrders();


        $page = $this->templating->render('observation/observation_list.html.twig', [
            'titre_page' => 'Liste des observations',
            'observations' => $pager,
            'orders' => $orders,
        ]);

        return new Response($page);
    }

    /**
     * @param string $id
     * @return Response
     * @Route("/show/{id}", name="observation_show")
     * @Method("GET")
     */
    public function showApprovedObservationAction(string $id): Response
    {
        $observation = $this->observationManager->findOneById($id);
        if (null === $observation || Observation::STATUS_APPROVED !== $observation->getStatus()) {
            throw new NotFoundHttpException();
        }

        $page = $this->templating->render('observation/observation_show.html.twig', [
            'observation' => $observation,
        ]);

        return new Response($page);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/list/pending", name="observations_pending_approval")
     * @Method("GET")
     * @Security("has_role('ROLE_NATURALIST')")
     */
    public function listAllObservationsPendingApprovalAction(Request $request): Response
    {
        $currentPage = (int) $request->query->get('page', 1);
        $pager = $this->observationManager->listAllObservationsPendingApprovalWithPagination($request->query->all());
        $pager->setMaxPerPage(25);
        $pager->setCurrentPage($currentPage);

        $orders = $this->specieManager->findOrders();

        $page = $this->templating->render('observation/observation_list.html.twig', [
            'titre_page' => 'Liste des observations en attente d’approbation',
            'observations' => $pager,
            'orders' => $orders
        ]);

        return new Response($page);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/list/refused", name="observations_refused")
     * @Method("GET")
     * @Security("has_role('ROLE_NATURALIST')")
     */
    public function listAllRefusedObservationsAction(Request $request): Response
    {
        $currentPage = (int) $request->query->get('page', 1);
        $pager = $this->observationManager->listAllRefusedObservationsWithPagination($request->query->all());
        $pager->setMaxPerPage(25);
        $pager->setCurrentPage($currentPage);

        $orders = $this->specieManager->findOrders();

        $page = $this->templating->render('observation/observation_list.html.twig', [
            'titre_page' => 'Liste des observations refusées',
            'observations' => $pager,
            'orders' => $orders
        ]);

        return new Response($page);
    }

    /**
     * @param string $id
     * @return Response
     * @Route("/list/pending/{id}/approve", requirements={"id" = "[a-z0-9\-]{36}"}, name="observation_approve")
     * @Method("GET")
     * @Security("has_role('ROLE_NATURALIST')")
     */
    public function approveObservationAction(string $id): Response
    {
        $observation = $this->observationManager->findOneById($id);
        if (null === $observation) {
            throw new NotFoundHttpException(sprintf('L’observation avec l’identifiant "%s" n’existe pas ou est déjà approuvée.', $id));
        }

        $approver = $this->tokenStorage->getToken()->getUser();
        $observation->setApprover($approver);
        $observation->setStatus(Observation::STATUS_APPROVED);
        $this->observationManager->save($observation);

        $this->session->getFlashBag()->add('success', 'L’observation a bien été approuvée.');
        $uri = $this->router->generate('observations_pending_approval');

        return new Response('', 302, ['location' => $uri]);
    }

    /**
     * @param string $id
     * @return Response
     * @Route("/list/pending/{id}/refuse", requirements={"id" = "[a-z0-9\-]{36}"}, name="observation_refuse")
     * @Method("GET")
     * @Security("has_role('ROLE_NATURALIST')")
     */
    public function refuseObservationAction(string $id): Response
    {
        $observation = $this->observationManager->findOneById($id);
        if (null === $observation) {
            throw new NotFoundHttpException(sprintf('L’observation avec l’identifiant "%s" n’existe pas ou est déjà approuvée.', $id));
        }

        $approver = $this->tokenStorage->getToken()->getUser();
        $observation->setApprover($approver);
        $observation->setStatus(Observation::STATUS_DISAPPROVED);
        $this->observationManager->save($observation);

        $this->session->getFlashBag()->add('success', 'L’observation a bien été refusée.');
        $uri = $this->router->generate('observations_pending_approval');

        return new Response('', 302, ['location' => $uri]);
    }

    /**
     * @param string $id
     * @return Response
     * @Route("/list/pending/{id}/delete", requirements={"id" = "[a-z0-9\-]{36}"}, name="observation_delete")
     * @Method("GET")
     * @Security("has_role('ROLE_NATURALIST')")
     */
    public function deleteObservationAction(string $id): Response
    {
        $observation = $this->observationManager->findOneById($id);
        if (null === $observation) {
            throw new NotFoundHttpException(sprintf('L’observation avec l’identifiant "%s" n’existe pas.', $id));
        }

        $this->observationManager->delete($observation);

        $this->session->getFlashBag()->add('success', 'L’observation a bien été supprimée.');
        $uri = $this->router->generate('observations_refused');

        return new Response('', 302, ['location' => $uri]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/add", name="observation_add")
     * @Method("GET|POST")
     * @Security("has_role('ROLE_USER')")
     */
    public function addObservationAction(Request $request): Response
    {
        $observation = new Observation();
        $user = $this->tokenStorage->getToken()->getUser();
        $form = $this->observationFormHandler->prepareForm($request, $observation);
        if ($this->observationFormHandler->processForm($form, $user)) {
            $this->session->getFlashBag()->add('success', 'Votre observation a bien été enregistrée. Elle sera vérifiée et disponible sous peu de temps.');
            $uri = $this->router->generate('observation_list');

            return new Response('', 302, ['location' => $uri]);
        }

        $page = $this->templating->render('observation/observation_add.html.twig', [
            'form' => $form->createView(),
        ]);

        return new Response($page);
    }

    /**
     * @param string $id
     * @param Request $request
     * @return Response
     * @Route("/edit/{id}", requirements={"id" = "[a-z0-9\-]{36}"}, name="observation_edit")
     * @Method("GET|POST")
     * @Security("has_role('ROLE_NATURALIST')")
     */
    public function editObservationAction(string $id, Request $request): Response
    {
        $observation = $this->observationManager->findOneById($id);
        if (null === $observation) {
            throw new NotFoundHttpException(sprintf('L’observation avec l’identifiant "%s" n’existe pas ou est déjà approuvée.', $id));
        }

        $user = $this->tokenStorage->getToken()->getUser();
        $form = $this->observationFormHandler->prepareForm($request, $observation);
        if ($this->observationFormHandler->processForm($form, $user)) {
            $this->session->getFlashBag()->add('success', 'L’observation a bien été modifiée.');
            $uri = $this->router->generate('observations_pending_approval');

            return new Response('', 302, ['location' => $uri]);
        }

        $page = $this->templating->render('observation/observation_edit.html.twig', [
            'form' => $form->createView(),
        ]);

        return new Response($page);
    }
}
