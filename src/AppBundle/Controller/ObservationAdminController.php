<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ObservationManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Templating\EngineInterface;

/**
 * @Route("/admin/observation")
 */
final class ObservationAdminController
{
    /**
     * @var ObservationManager
     */
    private $observationManager;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * ObservationAdminController constructor.
     *
     * @param ObservationManager $observationManager
     * @param EngineInterface $templating
     */
    public function __construct(ObservationManager $observationManager, EngineInterface $templating)
    {
        $this->observationManager = $observationManager;
        $this->templating = $templating;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/list", name="admin_observation_list")
     * @Method("GET")
     */
    public function listAllObservationsAction(Request $request): Response
    {
        $currentPage = (int) $request->query->get('page', 1);
        $pager = $this->observationManager->listAllObservationsWithPagination($request->query->all());
        $pager->setMaxPerPage(25);
        $pager->setCurrentPage($currentPage);

        $page = $this->templating->render('admin/observation_list.html.twig', [
            'observations' => $pager,
        ]);

        return new Response($page);
    }
}
