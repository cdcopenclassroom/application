<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Templating\EngineInterface;

/**
 * @Route("/admin/user")
 */
final class UserController
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * UserController constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param RouterInterface $router
     * @param UserManager $userManager
     * @param EngineInterface $templating
     */
    public function __construct(TokenStorageInterface $tokenStorage, RouterInterface $router, UserManager $userManager, EngineInterface $templating)
    {
        $this->tokenStorage = $tokenStorage;
        $this->userManager = $userManager;
        $this->templating = $templating;
        $this->router = $router;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/list", name="admin_user_list")
     * @Method("GET")
     */
    public function listUserAction(Request $request): Response
    {
        $currentPage = (int) $request->query->get('page', 1);
        $pager = $this->userManager->listAllUsersWithPagination();
        $pager->setMaxPerPage(25);
        $pager->setCurrentPage($currentPage);

        $page = $this->templating->render('admin/user_list.html.twig', [
            'users' => $pager,
        ]);

        return new Response($page);
    }

    /**
     * @param User $user
     * @return Response
     * @Route("/show/{username}", name="admin_user_view_profile")
     * @Method("GET")
     */
    public function showUserProfileAction(User $user): Response
    {
        $page = $this->templating->render('admin/user_profile.html.twig', [
            'user' => $user,
        ]);

        return new Response($page);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return Response
     * @Route("/disable/{username}", name="admin_user_disable")
     * @Method("GET")
     */
    public function disableUserAction(Request $request, User $user): Response
    {
        if ($this->tokenStorage->getToken()->getUser()->getId() !== $user->getId()) {
            $user->setEnabled(false);
            $this->userManager->updateUser($user);
        }
        $referer = $request->headers->get('referer');
        $uri = $referer ?? $this->router->generate('admin_user_list');

        return new Response('', 302, ['location' => $uri]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return Response
     * @Route("/disable/{username}", name="admin_user_enable")
     * @Method("GET")
     */
    public function enableUserAction(Request $request, User $user): Response
    {
        if ($this->tokenStorage->getToken()->getUser()->getId() !== $user->getId()) {
            $user->setEnabled(true);
            $this->userManager->updateUser($user);
        }
        $referer = $request->headers->get('referer');
        $uri = $referer ?? $this->router->generate('admin_user_list');

        return new Response('', 302, ['location' => $uri]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return Response
     * @Route("/promote/{username}", name="admin_user_promote")
     * @Method("GET")
     */
    public function promoteUserAction(Request $request, User $user): Response
    {
        if (!$user->hasRole('ROLE_NATURALIST')) {
            $user->addRole('ROLE_NATURALIST');
            $this->userManager->updateUser($user);
        }
        $referer = $request->headers->get('referer');
        $uri = $referer ?? $this->router->generate('admin_user_list');

        return new Response('', 302, ['location' => $uri]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return Response
     * @Route("/demote/{username}", name="admin_user_demote")
     * @Method("GET")
     */
    public function demoteUserAction(Request $request, User $user): Response
    {
        if ($user->hasRole('ROLE_NATURALIST')) {
            $user->removeRole('ROLE_NATURALIST');
            $this->userManager->updateUser($user);
        }
        $referer = $request->headers->get('referer');
        $uri = $referer ?? $this->router->generate('admin_user_list');

        return new Response('', 302, ['location' => $uri]);
    }
}
