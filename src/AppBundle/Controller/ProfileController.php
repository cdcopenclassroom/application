<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Templating\EngineInterface;

final class ProfileController
{
    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * ProfileController constructor.
     *
     * @param EngineInterface $templating
     */
    public function __construct(EngineInterface $templating)
    {
        $this->templating = $templating;
    }

    /**
     * @param User $user
     * @return Response
     * @Route("/user/{username}", name="user_profile")
     * @Method("GET")
     */
    public function profileAction(User $user): Response
    {
        $page = $this->templating->render('profile/profile.html.twig', ['user' => $user]);

        return new Response($page);
    }
}
