<?php

namespace AppBundle\Form\Handler;

use AppBundle\Entity\Observation;
use AppBundle\Entity\ObservationManager;
use AppBundle\Entity\User;
use AppBundle\Form\Type\ObservationFormType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

final class ObservationFormHandler
{
    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var ObservationManager
     */
    private $observationManager;

    /**
     * ObservationFormHandler constructor.
     *
     * @param FormFactory        $formFactory
     * @param ObservationManager $observationManager
     */
    public function __construct(FormFactory $formFactory, ObservationManager $observationManager)
    {
        $this->formFactory = $formFactory;
        $this->observationManager = $observationManager;
    }

    /**
     * @param Request          $request
     * @param Observation|null $data
     * @param array            $options
     *
     * @return FormInterface
     */
    public function prepareForm(Request $request, ?Observation $data = null, array $options = []): FormInterface
    {
        $form = $this->formFactory->create(ObservationFormType::class, $data, $options);
        $form->handleRequest($request);

        return $form;
    }

    /**
     * @param FormInterface $form
     * @param User          $user
     *
     * @return bool
     */
    public function processForm(FormInterface $form, User $user): bool
    {
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Observation $observation */
            $observation = $form->getData();
            $observation->setUser($user);
            $this->observationManager->save($observation);

            return true;
        }

        return false;
    }
}
