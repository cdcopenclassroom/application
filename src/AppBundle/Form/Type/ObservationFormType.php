<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Observation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TaxRefBundle\Entity\Specie;
use TaxRefBundle\Entity\SpecieRepository;

final class ObservationFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('specie', EntityType::class, [
            'label' => 'Espèce',
            'class' => Specie::class,
            'query_builder' => function(SpecieRepository $er) {
                return $er->findAllUnique();
            },
        ]);

        $builder->add('number', IntegerType::class, [
            'label' => 'Nombre',
        ]);

        $builder->add('latitude', TextType::class, [
            'label' => 'Latitude',
        ]);
        $builder->add('longitude', TextType::class, [
            'label' => 'Longitude',
        ]);

        
        $builder->add('viewed_at', DateTimeType::class, [
            'label' => 'Observation réalisée le',
        ]);

        $builder->add('comment', TextareaType::class, [
            'label' => 'Remarque',
            'required' => false,
        ]);
        
        
        
        
        $builder->add('file', FileType::class, [
            'required' => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Observation::class,
        ));
    }
}
