<?php
namespace AppBundle\Handler;

use AppBundle\Annotation\UploadableField;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

final class UploadHandler
{
    /**
     * @var PropertyAccessor
     */
    private $accessor;

    /**
     * UploadHandler constructor.
     */
    public function __construct()
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * @param $entity
     * @param string $property
     * @param UploadableField $annotation
     */
    public function uploadFile($entity, string $property, UploadableField $annotation)
    {
        $file = $this->accessor->getValue($entity, $property);
        if ($file instanceof UploadedFile) {
            $this->removeOldFile($entity, $annotation);
            $filename = Uuid::uuid4()->toString();
            $file->move($annotation->getPath(), $filename);
            $this->accessor->setValue($entity, $annotation->getFilename(), $filename);
        }
    }

    /**
     * @param $entity
     * @param string $property
     * @param UploadableField $annotation
     */
    public function setFileFromFilename($entity, string $property, UploadableField $annotation)
    {
        $file = $this->getFileFromFilename($entity, $annotation);
        $this->accessor->setValue($entity, $property, $file);
    }

    /**
     * @param $entity
     * @param UploadableField $annotation
     */
    public function removeOldFile($entity, UploadableField $annotation)
    {
        $file = $this->getFileFromFilename($entity, $annotation);
        if ($file !== null) {
            @unlink($file->getRealPath());
        }
    }

    /**
     * @param $entity
     * @param string $property
     */
    public function removeFile($entity, string $property)
    {
        $file = $this->accessor->getValue($entity, $property);
        if ($file instanceof File) {
            @unlink($file->getRealPath());
        }
    }

    /**
     * @param $entity
     * @param UploadableField $annotation
     *
     * @return null|File
     */
    private function getFileFromFilename ($entity, UploadableField $annotation): ?File
    {
        $filename = $this->accessor->getValue($entity, $annotation->getFilename());
        if (empty($filename)) {
            return null;
        } else {
            return new File($annotation->getPath() . DIRECTORY_SEPARATOR . $filename, false);
        }
    }

}
