<?php
namespace AppBundle\Annotation;

use Doctrine\Common\Annotations\AnnotationReader;

final class UploadAnnotationReader {

    /**
     * @var AnnotationReader
     */
    private $reader;

    /**
     * UploadAnnotationReader constructor.
     *
     * @param AnnotationReader $reader
     */
    public function __construct(AnnotationReader $reader)
    {
        $this->reader = $reader;
    }

    /**
     * @param $entity
     *
     * @return array
     */
    public function getUploadableFields($entity): array
    {
        $reflection = new \ReflectionClass(get_class($entity));
        $properties = [];
        foreach($reflection->getProperties() as $property) {
            $annotation = $this->reader->getPropertyAnnotation($property, UploadableField::class);
            if ($annotation !== null) {
                $properties[$property->getName()] = $annotation;
            }
        }
        return $properties;
    }

}
