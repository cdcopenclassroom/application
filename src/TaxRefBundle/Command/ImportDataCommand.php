<?php

namespace TaxRefBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use TaxRefBundle\Entity\Specie;
use TaxRefBundle\Entity\SpecieManager;

final class ImportDataCommand extends ContainerAwareCommand
{
    private $species = [];

    private const SPECIE_REGNE = 0;
    private const SPECIE_PHYLUM = 1;
    private const SPECIE_CLASSE = 2;
    private const SPECIE_ORDRE = 3;
    private const SPECIE_FAMILLE = 4;
    private const SPECIE_GROUP1_INPN = 5;
    private const SPECIE_GROUP2_INPN = 6;
    private const SPECIE_CD_NOM = 7;
    private const SPECIE_CD_TAXSUP = 8;
    private const SPECIE_CD_SUP = 9;
    private const SPECIE_CD_REF = 10;
    private const SPECIE_RANG = 11;
    private const SPECIE_LB_NOM = 12;
    private const SPECIE_LB_AUTEUR = 13;
    private const SPECIE_NOM_COMPLET_HTML = 15;
    private const SPECIE_NOM_VALIDE = 16;
    private const SPECIE_NOM_VERN = 17;
    private const SPECIE_NOM_VERN_ENG = 18;
    private const SPECIE_HABITAT = 19;
    private const SPECIE_URL = 37;

    protected function configure()
    {
        $this
            ->setName('taxref:import')
            ->setDescription('Importer les espèces depuis un fichier TaxRef.')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->loadSpecies($output);
        $output->writeln(PHP_EOL.'Processus terminé. '.count($this->species). ' espèces importées ou mises à jour.');
    }

    /**
     * @param OutputInterface $output
     *
     * @return void
     */
    private function loadSpecies(OutputInterface $output): void
    {
        $filename = $this->getContainer()->get('kernel')->locateResource('@TaxRefBundle/Resources/taxref/TAXREFv10.0 AVES.csv');
        $lines = $this->loadFile($filename);

        /** @var SpecieManager $specieManager */
        $specieManager = $this->getContainer()->get(SpecieManager::class);

        $output->writeln(PHP_EOL.PHP_EOL.'--------------------------------');
        $output->writeln('Importation des espèces.');
        $output->writeln('--------------------------------');

        $progress = new ProgressBar($output, count($lines));
        $progress->start();
        foreach ($lines as $k => $line) {
            if (0 < $k) {
                $line = explode(';', trim($line));
                $specie = $specieManager->findOneByCDNom((int)$line[self::SPECIE_CD_NOM]) ?? new Specie();
                $specie->setCdNom((int)$line[self::SPECIE_CD_NOM]);
                $specie->setCdRef(!empty($line[self::SPECIE_CD_REF]) ? (int)$line[self::SPECIE_CD_REF] : null);
                $specie->setCdSup(!empty($line[self::SPECIE_CD_SUP]) ? (int)$line[self::SPECIE_CD_SUP] : null);
                $specie->setCdTaxsup(!empty($line[self::SPECIE_CD_TAXSUP]) ? (int)$line[self::SPECIE_CD_TAXSUP] : null);
                $specie->setClass(!empty($line[self::SPECIE_CLASSE]) ? $line[self::SPECIE_CLASSE] : null);
                $specie->setEnglishVernacularName(!empty($line[self::SPECIE_NOM_VERN_ENG]) ? $line[self::SPECIE_NOM_VERN_ENG] : null);
                $specie->setFamily(!empty($line[self::SPECIE_FAMILLE]) ? $line[self::SPECIE_FAMILLE] : null);
                $specie->setGroup1(!empty($line[self::SPECIE_GROUP1_INPN]) ? $line[self::SPECIE_GROUP1_INPN] : null);
                $specie->setGroup2(!empty($line[self::SPECIE_GROUP2_INPN]) ? $line[self::SPECIE_GROUP2_INPN] : null);
                $specie->setHabitat(!empty($line[self::SPECIE_HABITAT]) ? $line[self::SPECIE_HABITAT] : null);
                $specie->setHtmlName(!empty($line[self::SPECIE_NOM_COMPLET_HTML]) ? $line[self::SPECIE_NOM_COMPLET_HTML] : null);
                $specie->setLbAuthor(!empty($line[self::SPECIE_LB_AUTEUR]) ? $line[self::SPECIE_LB_AUTEUR] : null);
                $specie->setLbName(!empty($line[self::SPECIE_LB_NOM]) ? $line[self::SPECIE_LB_NOM] : null);
                $specie->setOrdre(!empty($line[self::SPECIE_ORDRE]) ? $line[self::SPECIE_ORDRE] : null);
                $specie->setPhylum(!empty($line[self::SPECIE_PHYLUM]) ? $line[self::SPECIE_PHYLUM] : null);
                $specie->setRank(!empty($line[self::SPECIE_RANG]) ? $line[self::SPECIE_RANG] : null);
                $specie->setReign(!empty($line[self::SPECIE_REGNE]) ? $line[self::SPECIE_REGNE] : null);
                $specie->setUrl(!empty($line[self::SPECIE_URL]) ? $line[self::SPECIE_URL] : null);
                $specie->setValidatedName(!empty($line[self::SPECIE_NOM_VALIDE]) ? $line[self::SPECIE_NOM_VALIDE] : null);
                $specie->setVernacularName(!empty($line[self::SPECIE_NOM_VERN]) ? $line[self::SPECIE_NOM_VERN] : null);
                $this->species[self::SPECIE_CD_NOM] = $specie;
                $specieManager->save($specie, false);
                if (0 === $k%100) {
                    $specieManager->flush();
                }
            }

            $progress->advance();
        }

        $specieManager->flush();
        $progress->finish();
    }

    /**
     * @param string $filename
     *
     * @return array
     */
    private function loadFile(string $filename): array
    {
        if (!is_file($filename)) {
            throw new \InvalidArgumentException(sprintf('Impossible de trouver le fichier "%s" !', $filename));
        }

        return explode("\n", trim(file_get_contents($filename)));
    }
}
