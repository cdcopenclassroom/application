<?php

namespace TaxRefBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Templating\EngineInterface;
use TaxRefBundle\Entity\SpecieManager;

/**
 * @Route("/specie")
 */
final class SpecieController
{
    /**
     * @var SpecieManager
     */
    private $specieManager;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * SpecieController constructor.
     *
     * @param SpecieManager   $specieManager
     * @param EngineInterface $templating
     */
    public function __construct(SpecieManager $specieManager, EngineInterface $templating)
    {
        $this->specieManager = $specieManager;
        $this->templating = $templating;
    }

    /**
     * @param int $cd_nom
     *
     * @Route("/{cd_nom}", requirements={"cd_nom" = "\d+"}, name="specie_show")
     * @Method("GET")
     *
     * @return Response
     */
    public function showSpecieAction(int $cd_nom): Response
    {
        $specie = $this->specieManager->findOneByCDNom($cd_nom);
        if (null === $specie) {
            throw new NotFoundHttpException(sprintf('L’espèce avec le code "%d" est inconnue.', $cd_nom));
        }

        $page = $this->templating->render('@TaxRef/specie/show.html.twig', [
            'specie' => $specie,
        ]);

        return new Response($page);
    }

    /**
     * @param int $cd_nom
     *
     * @Route("/image/{cd_nom}", requirements={"cd_nom" = "\d+"}, name="specie_image_show")
     * @Method("GET")
     *
     * @return Response
     */
    public function showSpecieImageAction(int $cd_nom): Response
    {
        $filename = __DIR__ .'/../Resources/taxref/img/'.strval($cd_nom).'.jpg';
        if (!file_exists($filename)) {
            throw new NotFoundHttpException('Image not found');
        }

        $response = new Response();

        $response->headers->set('Content-type', mime_content_type($filename));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
        $response->headers->set('Content-length', filesize($filename));
        $response->setContent(file_get_contents($filename));

        return $response;
    }
}
