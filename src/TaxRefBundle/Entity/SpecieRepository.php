<?php

namespace TaxRefBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

final class SpecieRepository extends EntityRepository
{
    /**
     * @param string $name
     * @return Specie[]
     */
    public function findByName(string $name): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('s')
            ->from(Specie::class, 's')
            ->where('s.vernacular_name LIKE :name')
            ->OrWhere('s.english_vernacular_name LIKE :name')
            ->OrWhere('s.validated_name LIKE :name')
            ->setParameter('name', '%'.$name.'%');

        return $queryBuilder->getQuery()->execute();
    }

    /**
     * @return QueryBuilder
     */
    public function findAllUnique(): QueryBuilder
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('s')
            ->where('s.cd_nom = s.cd_ref')
            ->addOrderBy('s.vernacular_name')
            ->addOrderBy('s.validated_name')
            ->from(Specie::class, 's');

        return $queryBuilder;
    }

    /**
     * @return array
     */
    public function findOrders(): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('s.ordre')
            ->from(Specie::class, 's')
            ->groupBy('s.ordre')
            ->where('s.ordre IS NOT NULL');

        return array_column($queryBuilder
            ->getQuery()
            ->execute(), 'ordre');
    }

    /**
     * @param string $order
     *
     * @return array
     */
    public function findFamiliesFromOrder(string $order): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('s.family')
            ->from(Specie::class, 's')
            ->groupBy('s.family')
            ->where('s.ordre = :order')
            ->andWhere('s.family IS NOT NULL')
            ->setParameter('order', $order);

        return array_column($queryBuilder
            ->getQuery()
            ->execute(), 'family');
    }

    /**
     * @param string $family
     *
     * @return Specie[]
     */
    public function findSpeciesFromFamily(string $family): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('s')
            ->from(Specie::class, 's')
            ->where('s.family = :family')
            ->setParameter('family', $family);

        return $queryBuilder
            ->getQuery()
            ->execute();
    }

    /**
     * @param string $order
     *
     * @return Specie[]
     */
    public function findSpeciesFromOrder(string $order): array
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('s')
            ->from(Specie::class, 's')
            ->where('s.ordre = :order')
            ->setParameter('order', $order);

        return $queryBuilder
            ->getQuery()
            ->execute();
    }
}
