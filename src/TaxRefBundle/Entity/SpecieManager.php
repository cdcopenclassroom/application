<?php

namespace TaxRefBundle\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\ManagerRegistry;

final class SpecieManager
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var ManagerRegistry
     */
    private $registry;

    /**
     * SpecieManager constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = Specie::class;
        $this->registry = $registry;
    }

    /**
     * @return Specie[]
     */
    public function findAll(): array
    {
        return $this->getEntityRepository()->findAll();
    }

    /**
     * @param int $cd_nom
     *
     * @return Specie|null
     */
    public function findOneByCDNom(int $cd_nom): ?Specie
    {
        return $this->getEntityRepository()->findOneBy(['cd_nom' => $cd_nom]);
    }

    /**
     * @param string $name
     *
     * @return Specie[]
     */
    public function findByName(string $name): array
    {
        return $this->getEntityRepository()->findByName($name);
    }

    /**
     * @return array
     */
    public function findOrders(): array
    {
        return $this->getEntityRepository()->findOrders();
    }

    /**
     * @param string $order
     *
     * @return array
     */
    public function findFamiliesFromOrder(string $order): array
    {
        return $this->getEntityRepository()->findFamiliesFromOrder($order);
    }

    /**
     * @param string $family
     *
     * @return array
     */
    public function findSpeciesFromFamily(string $family): array
    {
        return $this->getEntityRepository()->findSpeciesFromFamily($family);
    }

    /**
     * @param string $order
     *
     * @return array
     */
    public function findSpeciesFromOrder(string $order): array
    {
        return $this->getEntityRepository()->findSpeciesFromOrder($order);
    }

    /**
     * @param Specie $specie
     * @param bool $flush
     *
     * @throws \Exception
     */
    public function save(Specie $specie, bool $flush = true)
    {
        try {
            $this->getEntityManager()->getConnection()->beginTransaction();
            $this->getEntityManager()->persist($specie);
            if ($flush) {
                $this->flush();
            }
            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            throw $e;
        }
    }

    /**
     *
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    /**
     * @return EntityManagerInterface
     */
    private function getEntityManager(): EntityManagerInterface
    {
        return $this->registry->getManagerForClass($this->class);
    }

    /**
     * @return SpecieRepository
     */
    private function getEntityRepository(): SpecieRepository
    {
        return $this->getEntityManager()->getRepository($this->class);
    }
}
