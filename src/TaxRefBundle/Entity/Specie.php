<?php

namespace TaxRefBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="species")
 * @ORM\Entity(repositoryClass="TaxRefBundle\Entity\SpecieRepository")
 */
class Specie implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer", unique=true)
     */
    protected $cd_nom;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $reign;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $phylum;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $class;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $ordre;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $family;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $group1;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $group2;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $cd_taxsup;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $cd_sup;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $cd_ref;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $rank;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lb_name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lb_author;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $html_name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $validated_name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $vernacular_name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $english_vernacular_name;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $habitat;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $url;

    /**
     * @return int
     */
    public function getCdNom(): int
    {
        return $this->cd_nom;
    }

    /**
     * @param int $cd_nom
     */
    public function setCdNom(int $cd_nom)
    {
        $this->cd_nom = $cd_nom;
    }

    /**
     * @return string
     */
    public function getReign(): ?string
    {
        return $this->reign;
    }

    /**
     * @param string $reign
     */
    public function setReign(?string $reign)
    {
        $this->reign = $reign;
    }

    /**
     * @return string
     */
    public function getPhylum(): ?string
    {
        return $this->phylum;
    }

    /**
     * @param string $phylum
     */
    public function setPhylum(?string $phylum)
    {
        $this->phylum = $phylum;
    }

    /**
     * @return string
     */
    public function getClass(): ?string
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass(?string $class)
    {
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getOrdre(): ?string
    {
        return $this->ordre;
    }

    /**
     * @param string $ordre
     */
    public function setOrdre(?string $ordre)
    {
        $this->ordre = $ordre;
    }

    /**
     * @return string
     */
    public function getFamily(): ?string
    {
        return $this->family;
    }

    /**
     * @param string $family
     */
    public function setFamily(?string $family)
    {
        $this->family = $family;
    }

    /**
     * @return string
     */
    public function getGroup1(): ?string
    {
        return $this->group1;
    }

    /**
     * @param string $group1
     */
    public function setGroup1(?string $group1)
    {
        $this->group1 = $group1;
    }

    /**
     * @return string
     */
    public function getGroup2(): ?string
    {
        return $this->group2;
    }

    /**
     * @param string $group2
     */
    public function setGroup2(?string $group2)
    {
        $this->group2 = $group2;
    }

    /**
     * @return int
     */
    public function getCdTaxsup(): ?int
    {
        return $this->cd_taxsup;
    }

    /**
     * @param int $cd_taxsup
     */
    public function setCdTaxsup(?int $cd_taxsup)
    {
        $this->cd_taxsup = $cd_taxsup;
    }

    /**
     * @return int
     */
    public function getCdSup(): ?int
    {
        return $this->cd_sup;
    }

    /**
     * @param int $cd_sup
     */
    public function setCdSup(?int $cd_sup)
    {
        $this->cd_sup = $cd_sup;
    }

    /**
     * @return int
     */
    public function getCdRef(): ?int
    {
        return $this->cd_ref;
    }

    /**
     * @param int $cd_ref
     */
    public function setCdRef(?int $cd_ref)
    {
        $this->cd_ref = $cd_ref;
    }

    /**
     * @return string
     */
    public function getRank(): ?string
    {
        return $this->rank;
    }

    /**
     * @param string $rank
     */
    public function setRank(?string $rank)
    {
        $this->rank = $rank;
    }

    /**
     * @return string
     */
    public function getLbName(): ?string
    {
        return $this->lb_name;
    }

    /**
     * @param string $lb_name
     */
    public function setLbName(?string $lb_name)
    {
        $this->lb_name = $lb_name;
    }

    /**
     * @return string
     */
    public function getLbAuthor(): ?string
    {
        return $this->lb_author;
    }

    /**
     * @param string $lb_author
     */
    public function setLbAuthor(?string $lb_author)
    {
        $this->lb_author = $lb_author;
    }

    /**
     * @return string
     */
    public function getHtmlName(): ?string
    {
        return $this->html_name;
    }

    /**
     * @param string $html_name
     */
    public function setHtmlName(?string $html_name)
    {
        $this->html_name = $html_name;
    }

    /**
     * @return string
     */
    public function getValidatedName(): ?string
    {
        return $this->validated_name;
    }

    /**
     * @param string $validated_name
     */
    public function setValidatedName(?string $validated_name)
    {
        $this->validated_name = $validated_name;
    }

    /**
     * @return string
     */
    public function getVernacularName(): ?string
    {
        return $this->vernacular_name;
    }

    /**
     * @param string $vernacular_name
     */
    public function setVernacularName(?string $vernacular_name)
    {
        $this->vernacular_name = $vernacular_name;
    }

    /**
     * @return string
     */
    public function getEnglishVernacularName(): ?string
    {
        return $this->english_vernacular_name;
    }

    /**
     * @param string $english_vernacular_name
     */
    public function setEnglishVernacularName(?string $english_vernacular_name)
    {
        $this->english_vernacular_name = $english_vernacular_name;
    }

    /**
     * @return int
     */
    public function getHabitat(): ?int
    {
        return $this->habitat;
    }

    /**
     * @return string
     */
    public function getHabitatName(): ?string
    {
        if (null !== $this->habitat) {
            return $this->getHabitatInformation($this->habitat)['name'];
        }
        return null;
    }

    /**
     * @return string
     */
    public function getHabitatDescription(): ?string
    {
        if (null !== $this->habitat) {
            return $this->getHabitatInformation($this->habitat)['description'];
        }
        return null;
    }

    /**
     * @param int $habitat
     */
    public function setHabitat(?int $habitat)
    {
        $this->habitat = $habitat;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(?string $url)
    {
        $this->url = $url;
    }

    public function jsonSerialize()
    {
        return [
            'cd_nom' => $this->getCdNom(),
            'cd_ref' => $this->getCdRef(),
            'cd_sup' => $this->getCdSup(),
            'cd_taxsup' => $this->getCdTaxsup(),
            'classe' => $this->getClass(),
            'nom_vern_eng' => $this->getEnglishVernacularName(),
            'famille' => $this->getFamily(),
            'group1_inpn' => $this->getGroup1(),
            'group2_inpn' => $this->getGroup2(),
            'habitat' => $this->getHabitat(),
            'nom_complet_html' => $this->getHtmlName(),
            'lb_auteur' => $this->getLbAuthor(),
            'lb_nom' => $this->getLbName(),
            'ordre' => $this->getOrdre(),
            'phylum' => $this->getPhylum(),
            'rang' => $this->getRank(),
            'regne' => $this->getReign(),
            'url' => $this->getUrl(),
            'nom_valide' => $this->getValidatedName(),
            'nom_vern' => $this->getVernacularName(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->getVernacularName() ?? $this->getValidatedName();
    }

    /**
     * @param int $code
     * @return array
     */
    private function getHabitatInformation(int $code): array
    {
        $habitats = [
            1 => [
                'name' => 'Marin ',
                'description' => 'Espèces vivant uniquement en milieu marin'
            ],
            2 => [
                'name' => 'Eau douce',
                'description' => 'Espèces vivant uniquement en milieu d’eau douce'],
            3 => [
                'name' => 'Terrestre',
                'description' => 'Espèces vivant uniquement en milieu terrestre'],
            4 => [
                'name' => 'Marin & Eau douce ',
                'description' => 'Espèces effectuant une partie de leur cycle de vie en eau douce et l’autre partie en mer (espèces diadromes, amphidromes, anadromes ou catadromes).'],
            5 => [
                'name' => 'Marin & Terrestre',
                'description' => 'Cas des pinnipèdes, des tortues et des oiseaux marins (par exemple).'],
            6 => [
                'name' => 'Eau saumâtre',
                'description' => 'Espèces vivant exclusivement en eau saumâtre.'],
            7 => [
                'name' => 'Continental (terrestre et/ou eau douce)',
                'description' => 'Espèces continentales (non marines) dont on ne sait pas si elles sont terrestres et/ou d’eau douce (taxons provenant de Fauna Europaea).'],
            8 => [
                'name' => 'Continental (terrestre et eau douce)',
                'description' => 'Espèces terrestres effectuant une partie de leur cycle en eau douce (odonatespar exemple), ou fortement liées au milieu aquatique (loutre par exemple).'
            ],
        ];

        if (array_key_exists($code, $habitats)) {
            return $habitats[$code];
        }
        throw new \InvalidArgumentException('Habitat inconnu.');
    }
}
