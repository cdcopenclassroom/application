<?php

namespace RestBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use TaxRefBundle\Entity\SpecieManager;

/**
 * @Route("/api/species")
 */
final class SpecieController
{
    /**
     * @var SpecieManager
     */
    private $specieManager;

    /**
     * RestController constructor.
     *
     * @param SpecieManager $specieManager
     */
    public function __construct(SpecieManager $specieManager)
    {
        $this->specieManager = $specieManager;
    }

    /**
     * @param int $cd_nom
     *
     * @Route("/by_cd_nom/{cd_nom}", requirements={"cd_nom" = "\d+"}, name="api_specie")
     * @Method("GET")
     *
     * @return Response
     */
    public function getSpecieByCDNomAction(int $cd_nom): Response
    {
        $specie = $this->specieManager->findOneByCDNom($cd_nom);
        if (null === $specie) {
            throw new NotFoundHttpException(sprintf('L’espèce avec le code "%d" est inconnue.', $cd_nom));
        }

        return new Response(json_encode($specie), 200, ['content-type' => 'application/json']);
    }

    /**
     * @param string $name
     *
     * @Route("/by_name/{name}", name="api_species_by_name")
     * @Method("GET")
     *
     * @return Response
     */
    public function getSpecieByNameAction(string $name): Response
    {
        $species = $this->specieManager->findByName($name);

        return new Response(json_encode($species), 200, ['content-type' => 'application/json']);
    }

    /**
     * @Route("/orders", name="api_species_orders")
     * @Method("GET")
     *
     * @return Response
     */
    public function findOrdersAction(): Response
    {
        $orders = $this->specieManager->findOrders();

        return new Response(json_encode($orders), 200, ['content-type' => 'application/json']);
    }

    /**
     * @param string $order
     *
     * @Route("/families/{order}", name="api_species_families_from_orders", options = { "expose" = true })
     * @Method("GET")
     *
     * @return Response
     */
    public function findFamiliesFromOrderAction(string $order): Response
    {
        $families = $this->specieManager->findFamiliesFromOrder($order);

        return new Response(json_encode($families), 200, ['content-type' => 'application/json']);
    }

    /**
     * @param string $family
     *
     * @Route("/by_family/{family}", name="api_species_by_family")
     * @Method("GET")
     *
     * @return Response
     */
    public function findSpeciesFromFamilyAction(string $family): Response
    {
        $species = $this->specieManager->findSpeciesFromFamily($family);

        return new Response(json_encode($species), 200, ['content-type' => 'application/json']);
    }

    /**
     * @param string $order
     *
     * @Route("/by_order/{order}", name="api_species_by_order")
     * @Method("GET")
     *
     * @return Response
     */
    public function findSpeciesFromOrderAction(string $order): Response
    {
        $species = $this->specieManager->findSpeciesFromOrder($order);

        return new Response(json_encode($species), 200, ['content-type' => 'application/json']);
    }
}
