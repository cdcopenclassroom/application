<?php

namespace RestBundle\Controller;

use AppBundle\Entity\ObservationManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/api/observations")
 */
final class ObservationController
{
    /**
     * @var ObservationManager
     */
    private $observationManager;

    /**
     * ObservationRestController constructor.
     *
     * @param ObservationManager $observationManager
     */
    public function __construct(ObservationManager $observationManager)
    {
        $this->observationManager = $observationManager;
    }

    /**
     * @param string $order
     *
     * @Route("/with_order/{order}", name="api_observations_order")
     * @Method("GET")
     *
     * @return Response
     */
    public function listAllApprovedObservationsWithOrderAction(string $order): Response
    {
        $observations = $this->observationManager->listAllApprovedWithOrder($order);

        return new Response(json_encode($observations), 200, ['content-type' => 'application/json']);
    }

    /**
     * @param string $family
     *
     * @Route("/with_family/{family}", name="api_observations_family")
     * @Method("GET")
     *
     * @return Response
     */
    public function listAllApprovedObservationsWithFamilyAction(string $family): Response
    {
        $observations = $this->observationManager->listAllApprovedWithFamily($family);

        return new Response(json_encode($observations), 200, ['content-type' => 'application/json']);
    }

    /**
     * @param string $cd_nom
     *
     * @Route("/with_cd_nom/{cd_nom}", name="api_observations_cd_nom")
     * @Method("GET")
     *
     * @return Response
     */
    public function listAllApprovedObservationsWithCdNomAction(string $cd_nom): Response
    {
        $observations = $this->observationManager->listAllApprovedWithCdNom($cd_nom);

        return new Response(json_encode($observations), 200, ['content-type' => 'application/json']);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/filter", name="observation_filter")
     * @Method("GET")
     */
    public function listAllApprovedObservationsAction(Request $request): Response
    {
        $observations = $this->observationManager->listAllApprovedObservations($request->query->all());

        return new Response(json_encode($observations), 200, ['Content-Type' => 'application/json']);
    }

    /**
     * @param string $id
     *
     * @Route("/{id}", name="api_observation_id")
     * @Method("GET")
     *
     * @return Response
     */
    public function getObservationByIdAction(string $id): Response
    {
        $observation = $this->observationManager->findOneById($id);
        if (null === $observation) {
            throw new NotFoundHttpException('Observation inconnue.');
        }

        return new Response(json_encode($observation), 200, ['content-type' => 'application/json']);
    }
}
