<?php

namespace ContactBundle\Form\Handler;

use AppBundle\Entity\User;
use ContactBundle\Entity\Message;
use ContactBundle\Entity\MessageManager;
use ContactBundle\Form\Type\MessageFormType;
use SebastianBergmann\CodeCoverage\Report\PHP;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

final class MessageFormHandler
{
    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var MessageManager
     */
    private $messageManager;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * MessageFormHandler constructor.
     *
     * @param \Swift_Mailer $mailer
     * @param FormFactory $formFactory
     * @param MessageManager $messageManager
     */
    public function __construct(\Swift_Mailer $mailer, FormFactory $formFactory, MessageManager $messageManager)
    {
        $this->mailer = $mailer;
        $this->formFactory = $formFactory;
        $this->messageManager = $messageManager;
    }

    /**
     * @param Request $request
     * @param Message|null $data
     * @param User|null $user
     * @param Message|null $previous_message
     * @return FormInterface
     */
    public function prepareForm(Request $request, ?Message $data = null, ?User $user = null, ?Message $previous_message = null): FormInterface
    {
        $options = [
            'user' => $user,
            'message' => $previous_message,
        ];
        $form = $this->formFactory->create(MessageFormType::class, $data, $options);
        $form->handleRequest($request);

        return $form;
    }

    /**
     * @param FormInterface $form
     *
     * @param User|null $user
     * @param Message|null $previous_message
     * @return bool
     */
    public function processForm(FormInterface $form, ?User $user = null, ?Message $previous_message = null): bool
    {
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Message $message */
            $message = $form->getData();
            $message->setUser($user);
            $message->setPreviousEmail($previous_message);
            $this->messageManager->save($message);

            if (null !== $previous_message) {
                $this->sendEmail($message);
            }

            return true;
        }

        return false;

    }

    private function sendEmail(Message $message)
    {
        $message = (new \Swift_Message('[Nos Amis les Oiseaux] Réponse à votre message'))
            ->setFrom($message->getEmail(), $message->getName())
            ->setTo($message->getPreviousEmail()->getEmail(), $message->getPreviousEmail()->getName())
            ->setBody(
                $message->getContent().PHP_EOL.
                '----------------------------------'.PHP_EOL.
                'Votre message: '. $message->getPreviousEmail()->getTitle().PHP_EOL.PHP_EOL.PHP_EOL.
                $message->getPreviousEmail()->getContent(),
                'text/plain'
            )
        ;

        $this->mailer->send($message);
    }
}
