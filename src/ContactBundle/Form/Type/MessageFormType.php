<?php

namespace ContactBundle\Form\Type;

use AppBundle\Entity\User;
use ContactBundle\Entity\Message;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class MessageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (null === $options['user']) {
            $builder->add('name', TextType::class, [
                'label' => 'Nom',
            ]);
            $builder->add('email', EmailType::class, [
                'label' => 'Courriel',
            ]);
        }
        $builder->add('title', TextType::class, [
            'label' => 'Titre',
        ]);
        $builder->add('content', TextareaType::class, [
            'label' => 'Message',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Message::class,
                'user'       => null,
                'message'    => null,
            ])
            ->setAllowedTypes('user', [User::class, 'null'])
            ->setAllowedTypes('message', [Message::class, 'null'])
        ;
    }
}
