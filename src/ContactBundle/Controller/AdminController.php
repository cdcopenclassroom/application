<?php

namespace ContactBundle\Controller;

use ContactBundle\Entity\Message;
use ContactBundle\Entity\MessageManager;
use ContactBundle\Form\Handler\MessageFormHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Templating\EngineInterface;

/**
 * @Route("/admin/messages")
 */
final class AdminController
{
    /**
     * @var MessageManager
     */
    private $messageManager;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    private $messageFormHandler;

    /**
     * AdminController constructor.
     *
     * @param MessageFormHandler $messageFormHandler
     * @param TokenStorageInterface $tokenStorage
     * @param EngineInterface $templating
     * @param MessageManager $messageManager
     * @param Session $session
     * @param RouterInterface $router
     */
    public function __construct(MessageFormHandler $messageFormHandler, TokenStorageInterface $tokenStorage, EngineInterface $templating, MessageManager $messageManager, Session $session, RouterInterface $router)
    {
        $this->messageFormHandler = $messageFormHandler;
        $this->tokenStorage = $tokenStorage;
        $this->templating = $templating;
        $this->messageManager = $messageManager;
        $this->session = $session;
        $this->router = $router;
    }

    /**
     * @param Request $request
     *
     * @Route("/", name="admin_messages")
     * @Method("GET")
     *
     * @return Response
     */
    public function getMessagesAction(Request $request): Response
    {
        $currentPage = (int) $request->query->get('page', 1);
        $pager = $this->messageManager->listAllMessagesWithPagination();
        $pager->setMaxPerPage(25);
        $pager->setCurrentPage($currentPage);

        $page = $this->templating->render('@Contact/admin/list.html.twig', [
            'messages' => $pager,
        ]);

        return new Response($page);
    }

    /**
     * @param string $id
     *
     * @Route("/{id}", name="admin_read_message")
     * @Method("GET")
     *
     * @return Response
     */
    public function getMessageAction(string $id): Response
    {
        $message = $this->messageManager->findOneById($id);
        if (null === $message) {
            throw new NotFoundHttpException('Message inconnu.');
        }
        if (false === $message->isOpened()) {
            $message->setOpened(true);
            $this->messageManager->save($message);
        }

        $page = $this->templating->render('@Contact/admin/show.html.twig', [
            'message' => $message,
        ]);

        return new Response($page);
    }

    /**
     * @param string $id
     *
     * @Route("/{id}/delete", name="admin_delete_message")
     * @Method("GET")
     *
     * @return Response
     */
    public function deleteMessageAction(string $id): Response
    {
        $message = $this->messageManager->findOneById($id);
        if (null === $message) {
            throw new NotFoundHttpException('Message inconnu.');
        }

        $this->messageManager->delete($message);
        $this->session->getFlashBag()->add('success', 'Le message a bien été supprimé.');
        $uri = $this->router->generate('admin_messages');

        return new Response('', 302, ['location' => $uri]);
    }

    /**
     * @param string $id
     * @param Request $request
     *
     * @Route("/{id}/anwser", name="admin_answer_message")
     * @Method("GET|POST")
     *
     * @return Response
     */
    public function sendMessageAction(string $id, Request $request): Response
    {
        $token = $this->tokenStorage->getToken();
        $user = $token instanceof UsernamePasswordToken ? $token->getUser() : null;
        $previousMessage = $this->messageManager->findOneById($id);
        if (null === $previousMessage) {
            throw new NotFoundHttpException('Message inconnu.');
        }
        $form = $this->messageFormHandler->prepareForm($request, new Message(), $user, $previousMessage);
        if ($this->messageFormHandler->processForm($form, $user, $previousMessage)) {
            $this->session->getFlashBag()->add('success', 'Votre message a bien été envoyé.');
            $uri = $this->router->generate('admin_read_message', ['id' => $previousMessage->getId()]);

            return new Response('', 302, ['location' => $uri]);
        }

        $page = $this->templating->render('@Contact/admin/answer.html.twig', [
            'form' => $form->createView(),
        ]);

        return new Response($page);
    }
}
