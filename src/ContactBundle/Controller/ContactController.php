<?php

namespace ContactBundle\Controller;

use ContactBundle\Entity\Message;
use ContactBundle\Form\Handler\MessageFormHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Templating\EngineInterface;

final class ContactController
{
    /**
     * @var MessageFormHandler
     */
    private $messageFormHandler;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * ContactController constructor.
     *
     * @param TokenStorage $tokenStorage
     * @param EngineInterface $templating
     * @param MessageFormHandler $messageFormHandler
     * @param Session $session
     * @param RouterInterface $router
     */
    public function __construct(TokenStorage $tokenStorage, EngineInterface $templating, MessageFormHandler $messageFormHandler, Session $session, RouterInterface $router)
    {
        $this->tokenStorage = $tokenStorage;
        $this->templating = $templating;
        $this->messageFormHandler = $messageFormHandler;
        $this->session = $session;
        $this->router = $router;
    }

    /**
     * @param Request $request
     *
     * @Route("/contact", name="contact")
     * @Method("GET|POST")
     *
     * @return Response
     */
    public function sendMessageAction(Request $request): Response
    {
        $token = $this->tokenStorage->getToken();
        $user = $token instanceof UsernamePasswordToken ? $token->getUser() : null;
        $message = new Message();

        $form = $this->messageFormHandler->prepareForm($request, $message, $user);
        if ($this->messageFormHandler->processForm($form, $user)) {

            $page = $this->templating->render('@Contact/message/message_success.html.twig');
            return new Response($page);
        }

        $page = $this->templating->render('@Contact/message/message.html.twig', [
            'form' => $form->createView(),
        ]);

        return new Response($page);
    }
}
