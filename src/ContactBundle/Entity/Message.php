<?php

namespace ContactBundle\Entity;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ContactBundle\Entity\MessageRepository")
 * @ORM\Table(name="messages")
 */
class Message implements \JsonSerializable
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    protected $content;

    /**
     * @var \Datetime|null
     *
     * @ORM\Column(type="datetime")
     */
    protected $sent_at;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $opened;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $user;

    /**
     * @var Message|null
     *
     * @ORM\OneToOne(targetEntity="Message", inversedBy="answer")
     * @ORM\JoinColumn(name="previous_email_id", referencedColumnName="id", nullable=true)
     */
    protected $previous_email;

    /**
     * @var Message|null
     *
     * @ORM\OneToOne(targetEntity="Message", mappedBy="previous_email")
     */
    protected $answer;

    /**
     * Message constructor.
     */
    public function __construct()
    {
        $this->sent_at = new \DateTime();
        $this->opened = false;
    }

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->getUser() ? $this->getUser()->getFullname() : $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser(?User $user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->getUser() ? $this->getUser()->getEmail() : $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return \Datetime|null
     */
    public function getSentAt(): ?\DateTime
    {
        return $this->sent_at;
    }

    /**
     * @param \Datetime|null $sent_at
     */
    public function setSentAt(?\DateTime $sent_at)
    {
        $this->sent_at = $sent_at;
    }

    /**
     * @return bool
     */
    public function isOpened(): bool
    {
        return $this->opened;
    }

    /**
     * @param bool $opened
     */
    public function setOpened(bool $opened)
    {
        $this->opened = $opened;
    }

    /**
     * @return Message|null
     */
    public function getPreviousEmail(): ?Message
    {
        return $this->previous_email;
    }

    /**
     * @param Message|null $previous_email
     */
    public function setPreviousEmail(?Message $previous_email)
    {
        $this->previous_email = $previous_email;
    }

    /**
     * @return Message|null
     */
    public function getAnswer(): ?Message
    {
        return $this->answer;
    }

    /**
     * @param Message|null $answer
     */
    public function setAnswer(?Message $answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return bool
     */
    public function hasAnswer(): bool
    {
        return $this->answer !== null;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'user' => $this->getUser(),
            'name' => $this->getName(),
            'email' => $this->getEmail(),
            'content' => $this->getContent(),
            'sent_at' => $this->getSentAt(),
            'opened' => $this->isOpened(),
            'previous_email' => $this->getPreviousEmail(),
        ];
    }
}
