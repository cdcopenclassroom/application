<?php

namespace ContactBundle\Entity;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Pagerfanta\Pagerfanta;

final class MessageManager
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var ManagerRegistry
     */
    private $registry;

    /**
     * MessageManager constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        $this->class = Message::class;
        $this->registry = $registry;
    }

    /**
     * @return Message[]
     */
    public function findAll(): array
    {
        return $this->getEntityRepository()->findAll();
    }

    /**
     * @param string $id
     * @return Message|null
     */
    public function findOneById(string $id): ?Message
    {
        return $this->getEntityRepository()->findOneBy(['id' => $id]);
    }

    /**
     * @return Pagerfanta
     */
    public function listAllMessagesWithPagination(): Pagerfanta
    {
        return $this->getEntityRepository()->listAllMessagesWithPagination();
    }

    /**
     * @param Message $message
     * @param bool $flush
     *
     * @throws \Exception
     */
    public function save(Message $message, bool $flush = true)
    {
        try {
            $this->getEntityManager()->getConnection()->beginTransaction();
            $this->getEntityManager()->persist($message);
            if ($flush) {
                $this->flush();
            }
            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            throw $e;
        }
    }

    /**
     * @param Message $message
     * @param bool $flush
     *
     * @throws \Exception
     */
    public function delete(Message $message, bool $flush = true)
    {
        try {
            $this->getEntityManager()->getConnection()->beginTransaction();
            $this->getEntityManager()->remove($message);
            if ($flush) {
                $this->flush();
            }
            $this->getEntityManager()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getEntityManager()->getConnection()->rollback();
            throw $e;
        }
    }

    /**
     *
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    /**
     * @return EntityManagerInterface
     */
    private function getEntityManager(): EntityManagerInterface
    {
        return $this->registry->getManagerForClass($this->class);
    }

    /**
     * @return MessageRepository
     */
    private function getEntityRepository(): MessageRepository
    {
        return $this->getEntityManager()->getRepository($this->class);
    }
}
