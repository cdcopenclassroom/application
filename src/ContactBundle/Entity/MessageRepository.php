<?php

namespace ContactBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

final class MessageRepository extends EntityRepository
{
    /**
     * @return Pagerfanta
     */
    public function listAllMessagesWithPagination(): Pagerfanta
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder()
            ->select('m')
            ->from(Message::class, 'm')
            ->where('m.previous_email IS NULL')
            ->orderBy('m.sent_at', 'DESC');
        $adapter = new DoctrineORMAdapter($queryBuilder);

        return new Pagerfanta($adapter);
    }
}
