<?php

require_once __DIR__.'/../vendor/autoload.php';

$dotenv = file_exists(__DIR__.'/../.env') ? __DIR__.'/../.env' : __DIR__.'/../.env.dist';
(new \Symfony\Component\Dotenv\Dotenv())->load( $dotenv);
